import javax.swing.*;  
class MenuSample
{  
          JMenu menu, submenu;  
          JMenuItem i1, i2, i3, i4, i5;  
          MenuSample(){  
          JFrame f= new JFrame("Menu and SubMenu");  
          JMenuBar mb=new JMenuBar();  
          menu=new JMenu("Menu");  
          submenu=new JMenu("Run");  
          i1=new JMenuItem("Open");  
          i2=new JMenuItem("Close");  
          i3=new JMenuItem("Save");  
          i4=new JMenuItem("Java App");  
          i5=new JMenuItem("Web App");  
          menu.add(i1); 
          menu.add(i2); 
          menu.add(i3);  
          submenu.add(i4); 
          submenu.add(i5);  
          menu.add(submenu);  
          mb.add(menu);  
          f.setJMenuBar(mb);  
          f.setSize(400,400);  
          f.setLayout(null);  
          f.setVisible(true);  
}  
public static void main(String args[])  
{  
new MenuSample();  
}}  