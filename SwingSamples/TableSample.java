
import javax.swing.*;    
public class TableSample {    
    JFrame f;    
    TableSample(){    
    f=new JFrame();    
    String data[][]={ {"1","John","20000"},    
                          {"2","Peter","30000"},    
                          {"3","Logan","25000"}};    
    String column[]={"ID","NAME","SALARY"};         
    JTable jt=new JTable(data,column);    
    jt.setBounds(30,40,200,300);          
    JScrollPane sp=new JScrollPane(jt);    
    f.add(sp);          
    f.setSize(300,400);    
    f.setVisible(true);    
}     
public static void main(String[] args) {    
    new TableSample();    
}    
}  