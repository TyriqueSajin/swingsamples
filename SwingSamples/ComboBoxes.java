
import javax.swing.*;    
public class ComboBoxes {    
JFrame f;    
ComboBoxes(){    
    f=new JFrame("ComboBox Example");    
    String city[]={"Bangalore","Chennai","Hyderabad","Delhi","Mumbai"};        
    JComboBox cb=new JComboBox(city);    
    cb.setBounds(50, 50,90,20);    
    f.add(cb);        
    f.setLayout(null);    
    f.setSize(400,500);    
    f.setVisible(true);         
}    
public static void main(String[] args) {    
    new ComboBoxes();         
}    
}   